# SPDX-FileCopyrightText: 2024 University of Washington
# SPDX-License-Identifier: MIT
#
# To install all required libraries, run
#      circup --path . install -r requirements.txt
#
"""CircuitPython blink example for built-in NeoPixel LED"""

import time
import board
import neopixel
from digitalio import DigitalInOut, Direction
import asyncio
import countio


pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)

# Define IO pins
#adcp_enable = DigitalInOut(board.A0)
sonar_enable_a = DigitalInOut(board.A1)
sonar_enable_b = DigitalInOut(board.A2)

# For counter I have to use PWM*B pins
# adcp_input_pin = board.MISO   # Original board used MISO, but it's a PWMA 
adcp_input_pin_one = board.MOSI
adcp_input_pin_two = board.A0

#adcp_input = DigitalInOut(adcp_input_pin)

sonar_trigger_a_pin = board.SCK
sonar_trigger_b_pin = board.RX

count = 0

# All enables have hardware pull-downs, so for now "enable" means hi-z
#adcp_enable.switch_to_input(None)
sonar_enable_a.switch_to_input(None)
sonar_enable_b.switch_to_input(None)

async def fake_sonar_trigger( sonar_period, sonar_event ):
    while True:
        await asyncio.sleep( sonar_period )
        sonar_event.set()

async def catch_adcp_trigger(pin, edge, name, sonar_event):
    """Print a message when pin goes low."""
    with countio.Counter(pin, edge=edge) as interrupt:
        while True:
            if interrupt.count > 0:
                interrupt.count = 0
                print(f"{name} edge!")

                pixel.fill((0, 255, 0))

                sonar_event.set()

            # Let another task run.
            await asyncio.sleep(0)


async def sonar_trigger(sonar_event, delay=0.03125, trigger_length=0.005):
    with DigitalInOut(sonar_trigger_a_pin) as trigger_a, DigitalInOut(sonar_trigger_b_pin) as trigger_b:
    
        trigger_a.switch_to_output(False)
        trigger_b.switch_to_output(False)
    
        while True:
        
            await sonar_event.wait()
            sonar_event.clear()

            await asyncio.sleep(delay)

            trigger_a.value = True
            pixel.fill((255, 0, 0))

            await asyncio.sleep(trigger_length)

            trigger_a.value = False
            pixel.fill((0, 0, 0))






async def main():

    sonar_event = asyncio.Event()

    # ADCP's ping schedule seems to be quantized in
    # integer units of 1/16th of a second.   Use
    # 1/32 of a second as "half an ADCP period"
    #
    #sonar_delay = 0.03125

    # No delay, use delay set in sonar itself
    sonar_delay = 0

    sonar_trigger_task = asyncio.create_task(sonar_trigger(sonar_event,sonar_delay))                                          

    # If true, board will generate an internal, fixed-period trigger (for testing)
    fake_trigger = True

    if fake_trigger:
        fake_sonar_period = 0.5 #seconds

        await asyncio.gather(sonar_trigger_task,
                asyncio.create_task(fake_sonar_trigger(fake_sonar_period, sonar_event)))

    else:

        await asyncio.gather(sonar_trigger_task,
                       asyncio.create_task(catch_adcp_trigger(adcp_input_pin_one, countio.Edge.FALL, "Rising", sonar_event)),
                       asyncio.create_task(catch_adcp_trigger(adcp_input_pin_two, countio.Edge.RISE, "Falling", sonar_event)))

asyncio.run(main())
